<?php
/*******************************************************************************
 * Projekt, Kurs: DT167G
 * File: password-reset.php
 * Desc: Page to reset password for members that forgot it
 *
 * Your name here
 ******************************************************************************/
include "includes/start.php"; // startup file

if(isset($_SESSION["user_id"])){ // if you are online you cant reset anything
	header("Location: index.php");
	die();
}

if(isset($_SESSION["resetWithPhone"]) && $_SESSION["resetWithPhone"] < time()){
	//$_SESSION["resetWithPhone"] = null;
	//unset($_SESSION["resetWithPhone"]);
	killItWithFire(); // just destroy the entire session to be sure, they are not logged in anyway
	$errorMes = "Times up, please try again";
}

$resetAvailable = resetAvailable();
if(!$resetAvailable){
	$errorMes = "To many attempts for reset, try again later.";
}elseif(isset($_POST["username"]) && isset($_POST["phone"])){
	$db = database_read::getInstance();
	$userResting = $db->getRow("member",array("name","phone"),array($_POST["username"],$_POST["phone"]));
	if($userResting){
		$_SESSION["resetWithPhone"] = time() + (3*60);
		$_SESSION["phoneCode"] = randomCode(4);
		$_SESSION["phoneNum"] = $userResting[0]["phone"];
		$_SESSION["resetId"] = $userResting[0]["id"];
	}else{ // wrong user or phone
		logfile("ResetpassNotFound", $_SERVER['REMOTE_ADDR']);
		
		$errorMes = "Wrong info"; // might be better to not say anything 
		$resetAvailable = resetAvailable();
		if(!$resetAvailable){
			$errorMes = "To many attempts for reset, try again later.";
		}
	}
}elseif(isset($_POST["phoneCode"]) && isset($_SESSION["resetWithPhone"])){
	if($_POST["phoneCode"] == $_SESSION["phoneCode"]){
		$_SESSION["resetWithPhone"] = time() + (3*60); // update timer so they have time to make a password
		$_SESSION["resetPass"] = true;
	}else{
		logfile("ResetpassBadCode", $_SERVER['REMOTE_ADDR']);
		killItWithFire(); // destroy all session variables to start over
		$errorMes ="Wrong code, please try again.";
		$resetAvailable = resetAvailable();
		if(!$resetAvailable){
			$errorMes = "To many attempts for reset, try again later.";
		}

	}
}elseif(isset($_SESSION["resetPass"]) && isset($_POST["newPass"])){ // password reset
	$check = checkPassword($_POST["newPass"]);
	if(!$check){
		$_SESSION["resetWithPhone"] == null;
		$_SESSION["resetPass"] == null;
		unset($_SESSION["resetWithPhone"]);
		unset($_SESSION["resetPass"]);
		$errorMes = "You password been reset!";
		$passwordReseted = true; // stop forms from showing
		$db = Database_admin::getInstance();
		$db->updateRow("member","id",$_SESSION["resetId"],"password",password_hash($_POST["newPass"], PASSWORD_BCRYPT));
	}else{
		// pass not meeting requirements
		$errorMes = $check;
		$_SESSION["resetWithPhone"] = time() + (3*60); // update timer so they have time to make a password

	}


}

/*******************************************************************************
 * HTML section starts here
 ******************************************************************************/
?>
<!DOCTYPE html>
<html lang="sv-SE">
<head>
	<?php include "includes/head.php" ?>
</head>
<body>

<?php include "includes/header.php"; ?>

<main>
	<!-- Main part for this page -->
	<h1>Password restore</h1>
	<div class="padder">
	<?php if(isset($errorMes)){ echo "<p>".$errorMes."</p>"; } ?>


	<?php
		if($resetAvailable && !isset($passwordReseted)){
			if(isset($_SESSION["resetPass"]) && $_SESSION["resetPass"] == true){ // form for new password
				 ?>
				<h2> Pick new password </h2>
				<form method="post">
					<label>New password</label>
					<input type="password" name="newPass" required>
					<button type="submit" name="newPassSubmit">Ok</button>
				</form>
				<?php
			} 
			elseif(isset($_SESSION["resetWithPhone"])){ // if values been added and phone code is needed
				echo "<script>smsPhone('".$_SESSION["phoneCode"]."','".$_SESSION["phoneNum"]."')</script>"; // fake phone!
				 ?>
				<h2> You will get an sms on your phone now </h2>
				<form method="post">
					<label>Phone code</label>
					<input type="text" name="phoneCode" required>
					<button type="submit" name="enterCodeSubmit">Ok</button>
				</form>
		<?php
			}else{ // if nothing is set	
		?>

				<form method="post">
					<label>Account name</label>
					<input type="text" name="username" required>
					<label>Phone number</label>
					<input type="number" placeholder="Only numbers" name="phone" required>
					<button type="submit" name="resetPassSubmit">Reset password</button>
				</form>

	<?php 
			} 
		}
	?>
	</div>
</main>

<?php include "includes/footer.php"; ?>

</body>
</html>
