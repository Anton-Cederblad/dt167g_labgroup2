/*******************************************************************************
 * Projekt, Kurs: DT167G
 * File: main.js
 * Desc: main JavaScript file for Secure Message site
 *
* Your name here
 *******************************************************************************
 * Other functions
 ******************************************************************************/

/*******************************************************************************
 * Main function
 ******************************************************************************/
function main() {


}
window.addEventListener("load", main, false); //Connect the main function to window load event

function smsPhone($code,$phone){
	//console.log($code);
	$("main").append('<div class="phone">'+
		'<p>Hello, this is a message from message-site by group2!'+
		' Sent to phone '+$phone+
		'. Your temporary code is: '+$code+'</p></div>');
	$(".phone").animate({right: "0px"}, 1000);

	// add click to remove phone
	$(".phone").click(function(){
		if($(this).css("right") == "0px"){
			$(".phone").animate({right: "-225px"}, 1000);
		}else{
			$(".phone").animate({right: "0px"}, 1000);
		}
	});
}