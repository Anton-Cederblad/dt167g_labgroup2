<?php
/*******************************************************************************
 * Projekt, Kurs: DT167G
 * File: database_admin.class.php
 * Desc: Class Databas connection with permission to sensitive data and deletion.
 * Only use this class when its really needed.
 *
 * Public functions.
 * getInstance()
 * close()
 * countRow($table, $whereArr = false, $isArr = false)
 * getRow($table, $whereArr = false, $isArr = false, $select = "*", $limit = 0, $orderby = false, $order = "asc")
 * insertRow($table, $nameArr, $valueArr)
 * deleteRow($table, $whereArr, $isArr)
 *
 * Erho0903
 ******************************************************************************/

class Database_admin extends Database {
	private static $instance2;

    public static function getInstance()
    {
        if (!isset(static::$instance2)) {
            static::$instance2 = new static;
            
            if(!static::$instance2->connect()){
                exit(); // connection error, kill the site for safty
                return false;
            }
        }
        return static::$instance2;
    }

	// Override
	protected function connect() {
		$settings = config::getInstance();
		$connectArray = $settings->getDBAdminConnect();

		// this is for pg connect
		/*
		//login name, login pass, server url, server port, database name, schematic name
		$conn_string = "host=".$connectArray[2]." port=".$connectArray[3]." dbname=".$connectArray[4]." user=".$connectArray[0]." password=".$connectArray[1];
		$this->schema = $valueArr[5];

        if ($this->db = pg_pconnect($conn_string)) {
            return TRUE;
	    }
		return FALSE;
		*/

		// this is for PDO 
		/*
		$connectString = 'pgsql :host='.$connectArray[2].';port='.$connectArray[3].';dbname='.$connectArray[4];	
		//echo "<h2>".$connectString."</h2>";
		
		$this->conn = new PDO($connectString, $connectArray[0], $connectArray[1]);
		if($this->conn->connect_error){
			echo $this->conn->connect_error;
			return false;
		}else{
			return true;
		}
		*/

		// this is for mysqli
		
		$this->conn = new mysqli($connectArray[2], $connectArray[0], $connectArray[1], $connectArray[4]); //connecting to database

        if (mysqli_connect_errno()) {
			printf("Connect failed: %s\n", mysqli_connect_error());
			return false;
		}	
		$this->conn->set_charset("utf8");

		return true;
    }


}
?>