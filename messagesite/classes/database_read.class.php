<?php
/*******************************************************************************
 * Projekt, Kurs: DT167G
 * File: database_read.class.php
 * Desc: Class Database for reading common tables.
 *
 * Public functions.
 * getInstance()
 * close()
 * countRow($table, $whereArr = false, $isArr = false)
 * getRow($table, $whereArr = false, $isArr = false, $select = "*", $limit = 0, $orderby = false, $order = "asc")
 * insertRow($table, $nameArr, $valueArr)
 * deleteRow($table, $whereArr, $isArr)
 *
 * Erho0903
 ******************************************************************************/

class Database_read extends Database {
	private static $instance3;

    public static function getInstance()
    {
        if (!isset(static::$instance3)) {
            static::$instance3 = new static;
            
            if(!static::$instance3->connect()){
                exit(); // connection error, kill the site for safty
                return false;
            }
        }
        return static::$instance3;
    }
	// Override
	protected function connect() {
		$settings = config::getInstance();
		// Connecting with an account that has limited access, only for reading some tables
		$connectArray = $settings->getDBReadConnect();

		$this->conn = new mysqli($connectArray[2], $connectArray[0], $connectArray[1], $connectArray[4]); //connecting to database

        if (mysqli_connect_errno()) {
			printf("Connect failed: %s\n", mysqli_connect_error());
			return false;
		}	
		$this->conn->set_charset("utf8");

		return true;
    }

   
    // Read only class, insert not allowed
 	public function insertRow($table, $nameArr, $valueArr) {
 		return false;
    }
    
    // Reading not allowed to delete
    public function deleteRow($table, $whereArr, $isArr) {
    	return false;
    }

     /**
     * Update row.
     */
    public function updateRow($table, $whereArr, $isArr, $setWhereArr, $setIsArr) {
        return false;
    }

}
?>