<?php
/*******************************************************************************
 * Projekt, Kurs: DT167G
 * File: new-member.php
 * Desc: Form and things to create new member
 *
 * ance
 ******************************************************************************/
include "includes/start.php"; // startup file

if(isset($_SESSION["user_id"])){
	header("Location: index.php");
	die();
}

$errormsg="";

if(isset($_POST['create-user']))
{
		$username = $_POST['regusername'];
		$phone = $_POST['regphone'];
		$password = $_POST['regpassword'];

		if(empty($username) || empty($phone) || empty($password))
		{
			$errormsg = "Error!";
		}
		else
		{
			if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response']))
			{

				$secret = '6LeItVsUAAAAAIG0HTP9rQM_N09CQL6cnDhFAAK_';
				$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);

				$responseData = json_decode($verifyResponse);

				if($responseData->success)
				{
					if(!is_numeric($phone))
					{
						$errormsg = "Phone number is not valid!";
					}
					else
					{
						$passcheck = checkPassword($password);
						$checkNew = checkNewMember($username);

						if(is_bool($passcheck))
						{
							if(is_bool($checkNew))
							{
								$validatecode = randomCode(4);
								$_SESSION['code'] = $validatecode;
								$_SESSION["reguname"] = $username;
								$_SESSION["regphone"] = $phone;
								$_SESSION["regpass"] = password_hash($password, PASSWORD_BCRYPT);
								$_SESSION["regtime"] = time() + (60 * 2);
								header("Location: validate-user.php");

							}
							else
							{
									$errormsg = $checkNew;
							}
						}
						else
						{
								$errormsg = $passcheck;
						}
					}
				}
				else
				{
					$errormsg = "Go away, stupid robot!";
				}
			}
			else
			{
				$errormsg = "Please click the captcha!";
			}
		}
	}
/*******************************************************************************
 * HTML section starts here
 ******************************************************************************/
?>
<!DOCTYPE html>
<html lang="sv-SE">
<head>
	<?php include "includes/head.php" ?>
</head>
<body>

<?php include "includes/header.php"; ?>

<main>
	<!-- Main part for this page -->

	<div class="formwrapper">
		<h2>
			Register new user
		</h2>
			<form method="post">
				<label>Username</label>
				<input type="text" name="regusername" required>
				<label>Phone number</label>
				<input type="text" placeholder="Only numbers" name="regphone" required>
				<label>Password</label>
				<input type="password" name="regpassword" required>
				<p>Min 8 chars, upper and lower char, symbol and non-common</p>
				<div class="g-recaptcha" data-sitekey="6LeItVsUAAAAAI-09F_PyWkijuEH2fIfzvqrhBTV"></div>
				<button type="submit" name="create-user">Create user</button>
				<?php
					if($errormsg != "")
					{
						echo '<p id="errormsg">'.$errormsg.'</p>';
					}
				?>
			</form>
	</div>
</main>

<?php include "includes/footer.php"; ?>

</body>
</html>
