<?PHP
/*******************************************************************************
 * Projekt, Kurs: DT167G
 * File: header.php
 * Desc: Header used by all sites
 *
 * Your name here
 ******************************************************************************/

?>
<header>
    <div class="wrapper">
        <div id="title">
                <a href="index.php"><h1>Message Site</h1></a>
        </div>
        <div id="logBox">
        
            <?php if(!isset($_SESSION["user"])){?>

            <div id="login">
                <form method="post">
                    <h2>LOGIN</h2>
                    <label>Username</label>
                    <input type="text" name="username" id="username" required>
                    <label>Password</label>
                    <input type="password" name="password" required>
                        <div id="buttons">
                            <button type="submit" id="loginSubmit" name="loginSubmit">Log in</button>
                            <a href="new-member.php" class="button">New user</a>
                            <a href="password-reset.php" class="button">Reset password</a>
                            <?php if(isset($loginError) && $loginError){
                                echo '<p class="error">'.$loginError.'</p>';
                            } ?>
                        </div>
                </form>
            </div>

            <?php }else{ ?>

            <div id="logout">
                <form method="post">
                      <h2>LOGOUT</h2>
                      <button type="submit" id="logoutButton" name="logoutButton">Logout</button>
                  </form>
              </div>

            <?php } ?>
        </div>
    </div>
    <hr>
</header>
