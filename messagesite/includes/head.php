<?PHP
/*******************************************************************************
 * Projekt, Kurs: DT167G
 * File: head.php
 * Desc: All includes used by all sites in head
 *
 * Your name here
 ******************************************************************************/


?>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Message site</title>
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
    <script src="js/main.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <link rel="stylesheet" href="css/style.css"/>
    <link rel="stylesheet" href="css/phone.css"/>
	<link rel="shortcut icon" href="img/favicon.ico">
